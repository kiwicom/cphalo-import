package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "cphalo-import",
	Short: "Generate Terraform config files from existing CloudPassage Halo resources.",
}

func init() {
	rootCmd.PersistentFlags().StringP("key", "k", "", "Application key")
	rootCmd.PersistentFlags().StringP("secret", "s", "", "Application secret")
	rootCmd.PersistentFlags().String("terraform-file", "", "Output file for Terraform config (leave blank for stdout)")
	rootCmd.PersistentFlags().String("script-file", "", "Output file for import shell script (leave blank for stdout)")
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
