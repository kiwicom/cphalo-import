package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/kiwicom/cphalo-import/pkg/output"
	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
	"gitlab.com/kiwicom/cphalo-import/pkg/remote"
)

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run importer and generate Terraform config files.",
	Run: func(cmd *cobra.Command, args []string) {
		key, secret, tfFile, shFile := getFlags(cmd)
		c := remote.NewClient(key, secret, nil)

		tfFileWriter := openFile(tfFile, false)
		defer tfFileWriter.Close()

		shFileWriter := openFile(shFile, true)
		defer shFileWriter.Close()

		var (
			state       = c.FetchState()
			parsedState = parser.ParseState(state)
		)

		if err := output.WriteState(key, secret, tfFileWriter, shFileWriter, os.Stdout, parsedState); err != nil {
			log.Fatalf("could not write parsed state: %v", err)
		}
	},
}

func getFlags(cmd *cobra.Command) (string, string, string, string) {
	key, err := cmd.Flags().GetString("key")
	if err != nil {
		log.Fatal(err)
	}
	if key == "" {
		log.Fatal("application key must be set")
	}

	secret, err := cmd.Flags().GetString("secret")
	if err != nil {
		log.Fatal(err)
	}
	if secret == "" {
		log.Fatal("application secret must be set")
	}

	tfFile, err := cmd.Flags().GetString("terraform-file")
	if err != nil {
		log.Fatal(err)
	}

	shFile, err := cmd.Flags().GetString("script-file")
	if err != nil {
		log.Fatal(err)
	}

	return key, secret, tfFile, shFile
}

func openFile(file string, exec bool) *os.File {
	if file == "" {
		return os.Stdout
	}

	handle, err := os.Create(file)
	if err != nil {
		log.Fatalf("could not open file: %v", err)
	}

	if exec {
		if err := handle.Chmod(0744); err != nil {
			handle.Close()
			log.Fatalf("could not make file executable: %v", err)
		}
	}

	return handle
}

func init() {
	rootCmd.AddCommand(runCmd)
}
