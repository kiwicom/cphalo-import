package main

import "gitlab.com/kiwicom/cphalo-import/internal/cmd"

func main() {
	cmd.Execute()
}
