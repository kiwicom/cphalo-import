package output

import (
	"fmt"
	"io"
	"log"
	"text/template"

	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

const firewallPolicyResource = `
{{- if .Policy.ErrorComment}}
/* {{.Policy.ErrorComment}}
{{- end}}
resource "cphalo_firewall_policy" "{{.Policy.SlugName}}" {
  name = "{{.Policy.Resource.Name}}"
  {{- if .Policy.Resource.Description}}
  description = "{{.Policy.Resource.Description}}"
  {{- end}}
  ignore_forwarding_rules = {{.Policy.Resource.IgnoreForwardingRules}}
  shared = {{.Policy.Resource.Shared}}

  {{- range $rule := .Policy.Rules}}
  rule {
    chain = "{{$rule.Resource.Chain}}"
    action = "{{$rule.Resource.Action}}"
    active = "{{$rule.Resource.Active}}"
    connection_states = "{{$rule.Resource.ConnectionStates}}"
    position = {{$rule.Resource.Position}}

    {{- if $rule.Resource.Log}}
    log = "{{$rule.Resource.Log}}"
	{{- end}}

    {{- if $rule.Resource.LogPrefix}}
    log_prefix = "{{$rule.Resource.LogPrefix}}"
	{{- end}}

    {{- if $rule.Resource.Comment}}
    comment = "{{$rule.Resource.Comment}}"
	{{- end}}

    {{- if $rule.Iface}}
    firewall_interface = "{{$rule.Iface.OutputID}}"
	{{- end}}
	
    {{- if $rule.Service}}
    firewall_service = "{{$rule.Service.OutputID}}"
	{{- end}}

    {{- if $rule.Source}}
    firewall_source {
      id = "{{$rule.Source.OutputID}}"
      kind = "{{$rule.Source.Kind}}"
    }
    {{- end}}

    {{- if $rule.Target}}
    firewall_target {
      id = "{{$rule.Target.OutputID}}"
      kind = "{{$rule.Target.Kind}}"
    }
    {{- end}}
  }
  {{- end}}
}
{{- if .Policy.ErrorComment}}
*/
{{- end}}
`

var (
	firewallPolicyResourceTemplate *template.Template
)

func init() {
	var err error

	firewallPolicyResourceTemplate, err = template.New("cphalo_firewall_zone_resource").Parse(firewallPolicyResource)
	if err != nil {
		log.Fatalf("could not parse firewall policy resource template: %v", err)
	}
}

func writeFirewallPolicies(w io.Writer, state parser.ParsedState, report *outputReport) error {
	for _, policy := range state.FirewallPolicies {
		if err := writeFirewallPolicy(w, policy, report); err != nil {
			return err
		}
	}

	return nil
}

func writeFirewallPolicy(w io.Writer, policy *parser.FirewallPolicyNode, report *outputReport) error {
	if policy.IsOutputted {
		return nil
	}

	// iterate over all possible resources attached to this policy and write them (just in case something was missed)
	for _, rule := range policy.Rules {
		if rule.Service != nil {
			if err := writeFirewallService(w, rule.Service, report); err != nil {
				return err
			}
		}
		if rule.Iface != nil {
			if err := writeFirewallInterface(w, rule.Iface, report); err != nil {
				return err
			}
		}
		if rule.Source != nil {
			if err := writeFirewallZone(w, rule.Source, report); err != nil {
				return err
			}
		}
		if rule.Target != nil {
			if err := writeFirewallZone(w, rule.Target, report); err != nil {
				return err
			}
		}
	}

	data := struct{ Policy parser.FirewallPolicyNode }{Policy: *policy}

	if err := firewallPolicyResourceTemplate.Execute(w, data); err != nil {
		return fmt.Errorf("could not render firewall policy (ID: %s) to template: %v", policy.Resource.ID, err)
	}

	if policy.ErrorComment == "" {
		appendImportStatement(fmt.Sprintf("cphalo_firewall_policy.%s", policy.SlugName), policy.Resource.ID)
		report.firewallPolicy.resourceGood++
	} else {
		report.firewallPolicy.resourceBad++
		report.hasSkipped = true
	}

	policy.IsOutputted = true

	return nil
}
