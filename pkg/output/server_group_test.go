package output

import (
	"testing"

	"gitlab.com/kiwicom/cphalo-go"
	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

func TestWriteServerGroups(t *testing.T) {
	testData := []testData{
		{
			writeServerGroups,
			parser.ParsedState{
				ServerGroups: []*parser.ServerGroupNode{
					{
						Resource: cphalo.ServerGroup{
							Name: "resource_name",
						},
						SlugName: "slugname",
						OutputID: "output_id",
					},
				},
			},
			"server-groups/01.tf",
		},
		{
			writeServerGroups,
			parser.ParsedState{
				ServerGroups: []*parser.ServerGroupNode{
					{
						Resource: cphalo.ServerGroup{
							Name:     "resource_name",
							ParentID: "parent_id",
						},
						Parent: &parser.ServerGroupNode{
							Resource: cphalo.ServerGroup{
								Name: "parent_resource_name",
							},
							SlugName: "parent_slugname",
							OutputID: "parent_output_id",
						},
						SlugName: "slugname",
						OutputID: "output_id",
					},
				},
			},
			"server-groups/02.tf",
		},
		{
			writeServerGroups,
			parser.ParsedState{
				ServerGroups: []*parser.ServerGroupNode{
					{
						IsOutputted: true,
					},
				},
			},
			"",
		},
	}

	execTests(t, testData)
}
