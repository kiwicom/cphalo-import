package output

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"testing"

	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

type testData struct {
	callee    func(writer io.Writer, state parser.ParsedState, report *outputReport) error
	state     parser.ParsedState
	checkFile string
}

func execTests(t *testing.T, data []testData) {
	report := new(outputReport)
	for _, tt := range data {
		var buffer bytes.Buffer

		t.Run("", func(t *testing.T) {
			if err := tt.callee(&buffer, tt.state, report); err != nil {
				t.Error(err)
			}

			if tt.checkFile == "" {
				if len(buffer.Bytes()) > 0 {
					t.Fatalf("output buffer should be empty, got: %s", buffer.String())
				}

				return
			}

			data, err := ioutil.ReadFile(fmt.Sprintf("testdata/%s", tt.checkFile))
			if err != nil {
				t.Fatalf("could not open file %s: %v", tt.checkFile, err)
			}

			trimmedExpected := bytes.TrimSpace(data)
			trimmedGot := bytes.TrimSpace(buffer.Bytes())

			if !bytes.Equal(trimmedExpected, trimmedGot) {
				t.Errorf("expected:\n%s\n", trimmedExpected)
				t.Errorf("got:\n%s\n", trimmedGot)
			}
		})
	}
}
