package output

import (
	"fmt"
	"io"
	"log"
	"text/template"

	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

const serverGroupFirewallPolicyResource = `
resource "cphalo_server_group_firewall_policy" "{{.Group.SlugName}}" {
  group_id = "{{.Group.Group.OutputID}}"
  linux_firewall_policy_id = "{{.Group.Policy.OutputID}}"
}
`

var (
	serverGroupFirewallPolicyResourceTemplate *template.Template
)

func init() {
	var err error

	serverGroupFirewallPolicyResourceTemplate, err = template.New("cphalo_server_group_firewall_policy_resource").Parse(serverGroupFirewallPolicyResource)
	if err != nil {
		log.Fatalf("could not parse server group resource template: %v", err)
	}
}

func writeServerGroupFirewallPolicies(w io.Writer, state parser.ParsedState, report *outputReport) error {
	for _, groupPolicy := range state.ServerGroupFirewallPolicies {
		if err := writeServerGroupFirewallPolicy(w, groupPolicy, report); err != nil {
			return err
		}
	}

	return nil
}

func writeServerGroupFirewallPolicy(w io.Writer, group *parser.ServerGroupFirewallPolicyNode, report *outputReport) error {
	if group.IsOutputted {
		return nil
	}

	data := struct {
		Group parser.ServerGroupFirewallPolicyNode
	}{Group: *group}

	if err := serverGroupFirewallPolicyResourceTemplate.Execute(w, data); err != nil {
		return fmt.Errorf("could not render server group firewall policy (ID: %s) to template: %v", group.Resource.GroupID, err)
	}

	appendImportStatement(fmt.Sprintf("cphalo_server_group_firewall_policy.%s", group.SlugName), group.Resource.GroupID)

	report.serverGroupFwPolicy.resourceGood++

	group.IsOutputted = true

	return nil
}
