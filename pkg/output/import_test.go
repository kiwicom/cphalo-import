package output

import (
	"bytes"
	"io/ioutil"
	"testing"
)

func TestWriteImportStatements(t *testing.T) {
	clearImportStatements()

	appendImportStatement("name1", "id1")
	appendImportStatement("name2", "id2")
	appendImportStatement("name3", "id3")
	appendImportStatement("name5", "id5")
	appendImportStatement("name4", "id4")

	var buffer bytes.Buffer

	if err := writeImportStatements(&buffer); err != nil {
		t.Error(err)
	}

	filePath := "testdata/import-statements/01.sh"
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		t.Fatalf("could not open file %s: %v", filePath, err)
	}

	trimmedExpected := bytes.TrimSpace(data)
	trimmedGot := bytes.TrimSpace(buffer.Bytes())

	if !bytes.Equal(trimmedExpected, trimmedGot) {
		t.Errorf("expected:\n%s\n", trimmedExpected)
		t.Errorf("got:\n%s\n", trimmedGot)
	}
}
