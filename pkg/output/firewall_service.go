package output

import (
	"fmt"
	"io"
	"log"
	"text/template"

	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

const firewallServiceResource = `
resource "cphalo_firewall_service" "{{.Service.SlugName}}" {
  name = "{{.Service.Resource.Name}}"
  protocol = "{{.Service.Resource.Protocol}}"
  {{- if .Service.Resource.Port}}
  port = "{{.Service.Resource.Port}}"
  {{- end}}
}
`

const firewallServiceData = `
data "cphalo_firewall_service" "{{.Service.SlugName}}" {
  name = "{{.Service.Resource.Name}}"
}
`

var (
	firewallServiceResourceTemplate *template.Template
	firewallServiceDataTemplate     *template.Template
)

func init() {
	var err error

	firewallServiceResourceTemplate, err = template.New("cphalo_firewall_service_resource").Parse(firewallServiceResource)
	if err != nil {
		log.Fatalf("could not parse firewall service resource template: %v", err)
	}

	firewallServiceDataTemplate, err = template.New("cphalo_firewall_service_data").Parse(firewallServiceData)
	if err != nil {
		log.Fatalf("could not parse firewall service data resource template: %v", err)
	}
}

func writeFirewallServices(w io.Writer, state parser.ParsedState, report *outputReport) error {
	for _, service := range state.FirewallServices {
		if err := writeFirewallService(w, service, report); err != nil {
			return err
		}
	}

	return nil
}

func writeFirewallService(w io.Writer, service *parser.FirewallServiceNode, report *outputReport) error {
	if service.IsOutputted {
		return nil
	}

	var tmpl *template.Template

	if service.Resource.System {
		tmpl = firewallServiceDataTemplate
	} else {
		tmpl = firewallServiceResourceTemplate
	}

	if err := tmpl.Execute(w, struct{ Service parser.FirewallServiceNode }{Service: *service}); err != nil {
		return fmt.Errorf("could not render firewall service (ID: %s) to template: %v", service.Resource.ID, err)
	}

	if !service.Resource.System {
		appendImportStatement(fmt.Sprintf("cphalo_firewall_service.%s", service.SlugName), service.Resource.ID)
		report.firewallService.resourceGood++
	} else {
		report.firewallService.dataGood++
	}

	service.IsOutputted = true

	return nil
}
