package output

import (
	"testing"

	"gitlab.com/kiwicom/cphalo-go"
	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

func TestWriteFirewallInterfaces(t *testing.T) {
	testData := []testData{
		{
			writeFirewallInterfaces,
			parser.ParsedState{
				FirewallInterfaces: []*parser.FirewallInterfaceNode{
					{
						Resource: cphalo.FirewallInterface{
							ID:   "resource_id",
							Name: "resource_name",
						},
						SlugName: "slugname",
						OutputID: "output_id",
						IsUsed:   true,
					},
				},
			},
			"fw-interfaces/01.tf",
		},
		{
			writeFirewallInterfaces,
			parser.ParsedState{
				FirewallInterfaces: []*parser.FirewallInterfaceNode{
					{
						Resource: cphalo.FirewallInterface{
							ID:     "resource_id",
							Name:   "resource_name",
							System: true,
						},
						SlugName: "slugname",
						OutputID: "output_id",
						IsUsed:   true,
					},
				},
			},
			"fw-interfaces/02.tf",
		},
		{
			writeFirewallInterfaces,
			parser.ParsedState{
				FirewallInterfaces: []*parser.FirewallInterfaceNode{
					{
						IsOutputted: true,
					},
				},
			},
			"",
		},
	}

	execTests(t, testData)
}
