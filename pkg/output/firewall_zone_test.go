package output

import (
	"testing"

	"gitlab.com/kiwicom/cphalo-go"
	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

func TestWriteFirewallZones(t *testing.T) {
	testData := []testData{
		{
			writeFirewallZones,
			parser.ParsedState{
				FirewallZones: []*parser.FirewallZoneNode{
					{
						Resource: cphalo.FirewallZone{
							Name:      "resource_name",
							IPAddress: "1.2.3.4",
						},
						SlugName:  "slugname",
						IPAddress: []string{"1.2.3.4"},
						OutputID:  "output_id",
					},
				},
			},
			"fw-zones/01.tf",
		},
		{
			writeFirewallZones,
			parser.ParsedState{
				FirewallZones: []*parser.FirewallZoneNode{
					{
						Resource: cphalo.FirewallZone{
							Name:        "resource_name",
							IPAddress:   "1.2.3.4",
							Description: "resource_description",
						},
						SlugName:  "slugname",
						IPAddress: []string{"1.2.3.4"},
						OutputID:  "output_id",
					},
				},
			},
			"fw-zones/02.tf",
		},
		{
			writeFirewallZones,
			parser.ParsedState{
				FirewallZones: []*parser.FirewallZoneNode{
					{
						Resource: cphalo.FirewallZone{
							Name:   "resource_name",
							System: true,
						},
						SlugName: "slugname",
						OutputID: "output_id",
					},
				},
			},
			"fw-zones/03.tf",
		},
		{
			writeFirewallZones,
			parser.ParsedState{
				FirewallZones: []*parser.FirewallZoneNode{
					{
						IsOutputted: true,
					},
				},
			},
			"",
		},
	}

	execTests(t, testData)
}
