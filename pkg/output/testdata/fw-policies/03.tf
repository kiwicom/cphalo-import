data "cphalo_firewall_service" "svc_slugname" {
  name = "firewall_service_name"
}

resource "cphalo_firewall_interface" "iface_slugname" {
  name = "firewall_interface_name"
}

resource "cphalo_firewall_zone" "source_fw_zone_slugname" {
  name = "source_fw_zone"
  description = "source_fw_zone_description"
  ip_address = [
    "4.3.2.1",
  ]
}

resource "cphalo_firewall_policy" "slugname" {
  name = "resource_name"
  ignore_forwarding_rules = true
  shared = true
  rule {
    chain = "INPUT"
    action = "ACCEPT"
    active = "false"
    connection_states = "NEW"
    position = 1
    firewall_interface = "iface_output_id"
    firewall_service = "svc_output_id"
    firewall_source {
      id = "source_fw_zone_output_id"
      kind = "FirewallZone"
    }
    firewall_target {
      id = "All Server Groups"
      kind = "Group"
    }
  }
}
