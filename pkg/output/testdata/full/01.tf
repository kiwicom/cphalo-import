provider "cphalo" {
  application_key    = "demo"
  application_secret = "test"
}

resource "cphalo_firewall_zone" "source_firewall_zone_slugname" {
  name = "source_fw_zone"
  description = "source_fw_zone_description"
  ip_address = [
    "4.3.2.1",
  ]
}

resource "cphalo_firewall_service" "firewall_service_slugname" {
  name = "firewall_service_name"
  protocol = "TCP"
  port = "22"
}

resource "cphalo_firewall_interface" "firewall_interface_slugname" {
  name = "firewall_interface_name"
}

resource "cphalo_firewall_policy" "firewall_policy_slugname" {
  name = "resource_name"
  ignore_forwarding_rules = true
  shared = true
  rule {
    chain = "INPUT"
    action = "ACCEPT"
    active = "false"
    connection_states = "NEW"
    position = 1
    firewall_interface = "firewall_interface_output_id"
    firewall_service = "firewall_service_output_id"
    firewall_source {
      id = "source_firewall_zone_output_id"
      kind = "FirewallZone"
    }
    firewall_target {
      id = "All Server Groups"
      kind = "Group"
    }
  }
}

/* no rules
resource "cphalo_firewall_policy" "empty_firewall_policy_slugname" {
  name = "resource_name"
  ignore_forwarding_rules = true
  shared = true
}
*/

data "cphalo_server_group" "server_group_parent_slugname" {
  name = "resource_name"
}

resource "cphalo_server_group" "server_group_slugname" {
  name = "resource_name"
  parent_id = "server_group_parent_output_id"
}

data "cphalo_alert_profile" "alert_profile_slugname" {
  name = "resource_name"
}
