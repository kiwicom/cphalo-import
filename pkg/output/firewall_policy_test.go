package output

import (
	"testing"

	"gitlab.com/kiwicom/cphalo-go"
	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

func TestWriteFirewallPolicies(t *testing.T) {
	testData := []testData{
		{
			writeFirewallPolicies,
			parser.ParsedState{
				FirewallPolicies: []*parser.FirewallPolicyNode{
					{
						Resource: cphalo.FirewallPolicy{
							Name: "resource_name",
						},
						SlugName: "slugname",
						OutputID: "output_id",
					},
				},
			},
			"fw-policies/01.tf",
		},
		{
			writeFirewallPolicies,
			parser.ParsedState{
				FirewallPolicies: []*parser.FirewallPolicyNode{
					{
						Resource: cphalo.FirewallPolicy{
							Name:                  "resource_name",
							IgnoreForwardingRules: true,
							Shared:                true,
						},
						SlugName: "slugname",
						OutputID: "output_id",
					},
				},
			},
			"fw-policies/02.tf",
		},
		{
			writeFirewallPolicies,
			parser.ParsedState{
				FirewallPolicies: []*parser.FirewallPolicyNode{
					{
						Resource: cphalo.FirewallPolicy{
							Name:                  "resource_name",
							IgnoreForwardingRules: true,
							Shared:                true,
						},
						Rules: []*parser.FirewallRuleNode{
							{
								Resource: cphalo.FirewallRule{
									Action:           "ACCEPT",
									Chain:            "INPUT",
									ConnectionStates: "NEW",
									Position:         1,
								},
								Service: &parser.FirewallServiceNode{
									Resource: cphalo.FirewallService{
										Name:   "firewall_service_name",
										System: true,
									},
									SlugName: "svc_slugname",
									OutputID: "svc_output_id",
								},
								Iface: &parser.FirewallInterfaceNode{
									Resource: cphalo.FirewallInterface{
										Name: "firewall_interface_name",
									},
									SlugName: "iface_slugname",
									OutputID: "iface_output_id",
								},
								Source: &parser.FirewallZoneNode{
									Resource: cphalo.FirewallZone{
										Name:        "source_fw_zone",
										IPAddress:   "4.3.2.1",
										Description: "source_fw_zone_description",
									},
									Kind:      "FirewallZone",
									SlugName:  "source_fw_zone_slugname",
									IPAddress: []string{"4.3.2.1"},
									OutputID:  "source_fw_zone_output_id",
								},
								Target: &parser.FirewallZoneNode{
									Resource: cphalo.FirewallZone{
										Name: "All Server Groups",
									},
									Kind:        "Group",
									OutputID:    "All Server Groups",
									IsOutputted: true,
								},
							},
						},
						SlugName: "slugname",
						OutputID: "output_id",
					},
				},
			},
			"fw-policies/03.tf",
		},
		{
			writeFirewallPolicies,
			parser.ParsedState{
				FirewallPolicies: []*parser.FirewallPolicyNode{
					{
						IsOutputted: true,
					},
				},
			},
			"",
		},
	}

	execTests(t, testData)
}
