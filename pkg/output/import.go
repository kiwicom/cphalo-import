package output

import (
	"fmt"
	"io"
)

var (
	importStatements []string
)

func appendImportStatement(name, id string) {
	importStatements = append(importStatements, fmt.Sprintf("terraform import %s %s", name, id))
}

func clearImportStatements() {
	importStatements = []string{}
}

func writeImportStatements(w io.Writer) error {
	if _, err := fmt.Fprint(w, "#!/usr/bin/env bash\n\n"); err != nil {
		return err
	}

	for _, statement := range importStatements {
		if _, err := fmt.Fprintf(w, "%s\n", statement); err != nil {
			return err
		}
	}

	return nil
}
