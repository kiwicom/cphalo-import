package output

import (
	"fmt"
	"io"
	"log"
	"text/template"

	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

const alertProfileData = `
data "cphalo_alert_profile" "{{.Alert.SlugName}}" {
  name = "{{.Alert.Resource.Name}}"
}
`

var (
	alertProfileDataTemplate *template.Template
)

func init() {
	var err error

	alertProfileDataTemplate, err = template.New("cphalo_alert_profile_data").Parse(alertProfileData)
	if err != nil {
		log.Fatalf("could not parse alert profile data resource template: %v", err)
	}
}

func writeAlertProfiles(w io.Writer, state parser.ParsedState, report *outputReport) error {
	for _, alert := range state.AlertProfiles {
		if err := writeAlertProfile(w, alert, report); err != nil {
			return err
		}
	}

	return nil
}

func writeAlertProfile(w io.Writer, alert *parser.AlertProfileNode, report *outputReport) error {
	if alert.IsOutputted {
		return nil
	}

	data := struct{ Alert parser.AlertProfileNode }{Alert: *alert}

	if err := alertProfileDataTemplate.Execute(w, data); err != nil {
		return fmt.Errorf("could not render alert profile (ID: %s) to template: %v", alert.Resource.ID, err)
	}

	report.alertProfile.dataGood++

	alert.IsOutputted = true

	return nil
}
