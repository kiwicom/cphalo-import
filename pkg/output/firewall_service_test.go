package output

import (
	"testing"

	"gitlab.com/kiwicom/cphalo-go"
	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

func TestWriteFirewallServices(t *testing.T) {
	testData := []testData{
		{
			writeFirewallServices,
			parser.ParsedState{
				FirewallServices: []*parser.FirewallServiceNode{
					{
						Resource: cphalo.FirewallService{
							Name:     "resource_name",
							Protocol: "ICMP",
						},
						SlugName: "slugname",
						OutputID: "output_id",
					},
				},
			},
			"fw-services/01.tf",
		},
		{
			writeFirewallServices,
			parser.ParsedState{
				FirewallServices: []*parser.FirewallServiceNode{
					{
						Resource: cphalo.FirewallService{
							Name:     "resource_name",
							Protocol: "TCP",
							Port:     "1234",
						},
						SlugName: "slugname",
						OutputID: "output_id",
					},
				},
			},
			"fw-services/02.tf",
		},
		{
			writeFirewallServices,
			parser.ParsedState{
				FirewallServices: []*parser.FirewallServiceNode{
					{
						Resource: cphalo.FirewallService{
							Name:   "resource_name",
							System: true,
						},
						SlugName: "slugname",
						OutputID: "output_id",
					},
				},
			},
			"fw-services/03.tf",
		},
		{
			writeFirewallServices,
			parser.ParsedState{
				FirewallServices: []*parser.FirewallServiceNode{
					{
						IsOutputted: true,
					},
				},
			},
			"",
		},
	}

	execTests(t, testData)
}
