package output

import (
	"fmt"
	"io"
	"log"
	"text/template"

	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

const firewallInterfaceResource = `
resource "cphalo_firewall_interface" "{{.Interface.SlugName}}" {
  name = "{{.Interface.Resource.Name}}"
}
`

const firewallInterfaceData = `
data "cphalo_firewall_interface" "{{.Interface.SlugName}}" {
  name = "{{.Interface.Resource.Name}}"
}
`

var (
	firewallInterfaceResourceTemplate *template.Template
	firewallInterfaceDataTemplate     *template.Template
)

func init() {
	var err error

	firewallInterfaceResourceTemplate, err = template.New("cphalo_firewall_interface_resource").Parse(firewallInterfaceResource)
	if err != nil {
		log.Fatalf("could not parse firewall interface resource template: %v", err)
	}

	firewallInterfaceDataTemplate, err = template.New("cphalo_firewall_interface_data").Parse(firewallInterfaceData)
	if err != nil {
		log.Fatalf("could not parse firewall interface data resource template: %v", err)
	}
}

func writeFirewallInterfaces(w io.Writer, state parser.ParsedState, report *outputReport) error {
	for _, iface := range state.FirewallInterfaces {
		if err := writeFirewallInterface(w, iface, report); err != nil {
			return err
		}
	}

	return nil
}

func writeFirewallInterface(w io.Writer, iface *parser.FirewallInterfaceNode, report *outputReport) error {
	if iface.IsOutputted {
		return nil
	}

	var tmpl *template.Template

	if iface.Resource.System {
		tmpl = firewallInterfaceDataTemplate
	} else {
		tmpl = firewallInterfaceResourceTemplate
	}

	if err := tmpl.Execute(w, struct{ Interface parser.FirewallInterfaceNode }{Interface: *iface}); err != nil {
		return fmt.Errorf("could not render firewall interface (ID: %s) to template: %v", iface.Resource.ID, err)
	}

	if !iface.Resource.System {
		appendImportStatement(fmt.Sprintf("cphalo_firewall_interface.%s", iface.SlugName), iface.Resource.ID)
		report.firewallInterface.resourceGood++
	} else {
		report.firewallInterface.dataGood++
	}

	iface.IsOutputted = true

	return nil
}
