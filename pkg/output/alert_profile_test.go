package output

import (
	"testing"

	"gitlab.com/kiwicom/cphalo-go"
	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

func TestWriteAlertProfiles(t *testing.T) {
	testData := []testData{
		{
			writeAlertProfiles,
			parser.ParsedState{
				AlertProfiles: []*parser.AlertProfileNode{
					{
						Resource: cphalo.AlertProfile{
							ID:   "resource_id",
							Name: "resource_name",
						},
						SlugName: "slugname",
						OutputID: "output_id",
					},
				},
			},
			"alert-profiles/01.tf",
		},
		{
			writeAlertProfiles,
			parser.ParsedState{
				AlertProfiles: []*parser.AlertProfileNode{
					{
						IsOutputted: true,
					},
				},
			},
			"",
		},
	}

	execTests(t, testData)
}
