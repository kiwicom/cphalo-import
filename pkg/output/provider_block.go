package output

import (
	"fmt"
	"io"
	"log"
	"text/template"
)

const providerBlock = `
provider "cphalo" {
  application_key    = "{{.Key}}"
  application_secret = "{{.Secret}}"
}
`

var (
	providerBlockTemplate *template.Template
)

func init() {
	var err error

	providerBlockTemplate, err = template.New("cphalo_provider").Parse(providerBlock)
	if err != nil {
		log.Fatalf("could not parse provider block template: %v", err)
	}
}

func writeProviderBlock(w io.Writer, key, secret string) error {
	data := struct {
		Key    string
		Secret string
	}{
		Key:    key,
		Secret: secret,
	}

	if err := providerBlockTemplate.Execute(w, data); err != nil {
		return fmt.Errorf("could not render provider block to template: %v", err)
	}

	return nil
}
