package output

import (
	"fmt"
	"io"
	"log"
	"text/template"

	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

const serverGroupResource = `
resource "cphalo_server_group" "{{.Group.SlugName}}" {
  name = "{{.Group.Resource.Name}}"
  {{- if .Group.Resource.Tag}}
  tag = "{{.Group.Resource.Tag}}"
  {{- end}}
  {{- if .Group.Resource.Description}}
  description = "{{.Group.Resource.Description}}"
  {{- end}}
  {{- if .Group.Resource.ParentID}}
  parent_id = "{{.Group.Parent.OutputID}}"
  {{- end}}
  {{- if .Group.Alerts}}
  alert_profile_ids = [
    {{- range $alert := .Group.Alerts}}
    "{{$alert.OutputID}}"
    {{- end}}
  ]
  {{- end}}
}
`

const serverGroupData = `
data "cphalo_server_group" "{{.Group.SlugName}}" {
  name = "{{.Group.Resource.Name}}"
}
`

var (
	serverGroupResourceTemplate *template.Template
	serverGroupDataTemplate     *template.Template
)

func init() {
	var err error

	serverGroupResourceTemplate, err = template.New("cphalo_server_group_resource").Parse(serverGroupResource)
	if err != nil {
		log.Fatalf("could not parse server group resource template: %v", err)
	}

	serverGroupDataTemplate, err = template.New("cphalo_server_group_data").Parse(serverGroupData)
	if err != nil {
		log.Fatalf("could not parse server group data resource template: %v", err)
	}
}

func writeServerGroups(w io.Writer, state parser.ParsedState, report *outputReport) error {
	for _, group := range state.ServerGroups {
		if err := writeServerGroup(w, group, report); err != nil {
			return err
		}
	}

	return nil
}

func writeServerGroup(w io.Writer, group *parser.ServerGroupNode, report *outputReport) error {
	if group.IsOutputted {
		return nil
	}

	var tmpl *template.Template

	if group.Resource.ParentID == "" {
		tmpl = serverGroupDataTemplate
	} else {
		tmpl = serverGroupResourceTemplate
	}

	if err := tmpl.Execute(w, struct{ Group parser.ServerGroupNode }{Group: *group}); err != nil {
		return fmt.Errorf("could not render server group (ID: %s) to template: %v", group.Resource.ID, err)
	}

	if group.Resource.ParentID != "" {
		appendImportStatement(fmt.Sprintf("cphalo_server_group.%s", group.SlugName), group.Resource.ID)
		report.serverGroup.resourceGood++
	} else {
		report.serverGroup.dataGood++
	}

	group.IsOutputted = true

	return nil
}
