package output

import (
	"testing"

	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

func TestWriteServerGroupFirewallPolicies(t *testing.T) {
	testData := []testData{
		{
			writeServerGroupFirewallPolicies,
			parser.ParsedState{
				ServerGroupFirewallPolicies: []*parser.ServerGroupFirewallPolicyNode{
					{
						Group: &parser.ServerGroupNode{
							OutputID: "server_group_output_id",
						},
						Policy: &parser.FirewallPolicyNode{
							OutputID: "firewall_policy_output_id",
						},
						SlugName: "slugname",
					},
				},
			},
			"server-group-firewall-policies/01.tf",
		},
		{
			writeServerGroupFirewallPolicies,
			parser.ParsedState{
				ServerGroups: []*parser.ServerGroupNode{
					{
						IsOutputted: true,
					},
				},
			},
			"",
		},
	}

	execTests(t, testData)
}
