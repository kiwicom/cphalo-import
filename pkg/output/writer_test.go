package output_test

import (
	"bytes"
	"io/ioutil"
	"testing"

	"gitlab.com/kiwicom/cphalo-go"
	"gitlab.com/kiwicom/cphalo-import/pkg/output"
	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

func TestWriteState(t *testing.T) {
	state := getMockState()

	var (
		terraformBuffer = new(bytes.Buffer)
		scriptBuffer    = new(bytes.Buffer)
		reportBuffer    = new(bytes.Buffer)
		key             = "demo"
		secret          = "test"
	)

	if err := output.WriteState(key, secret, terraformBuffer, scriptBuffer, reportBuffer, state); err != nil {
		t.Error(err)
	}

	compareOutput := func(filePath string, buf *bytes.Buffer) {
		data, err := ioutil.ReadFile(filePath)
		if err != nil {
			t.Fatalf("could not open file %s: %v", filePath, err)
		}

		exp := bytes.TrimSpace(data)
		got := bytes.TrimSpace(buf.Bytes())

		if !bytes.Equal(exp, got) {
			t.Errorf("expected:\n%s\n", exp)
			t.Errorf("got:\n%s\n", got)
		}
	}

	compareOutput("testdata/full/01.tf", terraformBuffer)
	compareOutput("testdata/full/01.sh", scriptBuffer)
	compareOutput("testdata/full/01.txt", reportBuffer)
}

func getMockState() parser.ParsedState {
	alertNode := &parser.AlertProfileNode{
		Resource: cphalo.AlertProfile{
			ID:   "alert_profile_resource_id",
			Name: "resource_name",
		},
		SlugName: "alert_profile_slugname",
		OutputID: "alert_profile_output_id",
	}

	svcNode := &parser.FirewallServiceNode{
		Resource: cphalo.FirewallService{
			ID:       "firewall_service_resource_id",
			Name:     "firewall_service_name",
			Protocol: "TCP",
			Port:     "22",
		},
		SlugName: "firewall_service_slugname",
		OutputID: "firewall_service_output_id",
	}

	ifaceNode := &parser.FirewallInterfaceNode{
		Resource: cphalo.FirewallInterface{
			ID:   "firewall_interface_resource_id",
			Name: "firewall_interface_name",
		},
		SlugName: "firewall_interface_slugname",
		OutputID: "firewall_interface_output_id",
	}

	zoneNode := &parser.FirewallZoneNode{
		Resource: cphalo.FirewallZone{
			ID:          "firewall_zone_resource_id",
			Name:        "source_fw_zone",
			IPAddress:   "4.3.2.1",
			Description: "source_fw_zone_description",
		},
		Kind:      "FirewallZone",
		SlugName:  "source_firewall_zone_slugname",
		IPAddress: []string{"4.3.2.1"},
		OutputID:  "source_firewall_zone_output_id",
	}

	policyNode := &parser.FirewallPolicyNode{
		Resource: cphalo.FirewallPolicy{
			ID:                    "firewall_policy_resource_id",
			Name:                  "resource_name",
			IgnoreForwardingRules: true,
			Shared:                true,
		},
		Rules: []*parser.FirewallRuleNode{
			{
				Resource: cphalo.FirewallRule{
					Action:           "ACCEPT",
					Chain:            "INPUT",
					ConnectionStates: "NEW",
					Position:         1,
				},
				Service: svcNode,
				Iface:   ifaceNode,
				Source:  zoneNode,
				Target: &parser.FirewallZoneNode{
					Resource: cphalo.FirewallZone{
						Name: "All Server Groups",
					},
					Kind:        "Group",
					OutputID:    "All Server Groups",
					IsOutputted: true,
				},
			},
		},
		SlugName: "firewall_policy_slugname",
		OutputID: "firewall_policy_output_id",
	}

	policyNodeEmpty := &parser.FirewallPolicyNode{
		Resource: cphalo.FirewallPolicy{
			ID:                    "empty_firewall_policy_resource_id",
			Name:                  "resource_name",
			IgnoreForwardingRules: true,
			Shared:                true,
		},
		ErrorComment: "no rules",
		SlugName:     "empty_firewall_policy_slugname",
		OutputID:     "empty_firewall_policy_output_id",
	}

	parentGroupNode := &parser.ServerGroupNode{
		Resource: cphalo.ServerGroup{
			ID:   "server_group_parent_resource_id",
			Name: "resource_name",
		},
		SlugName: "server_group_parent_slugname",
		OutputID: "server_group_parent_output_id",
	}

	groupNode := &parser.ServerGroupNode{
		Resource: cphalo.ServerGroup{
			ID:       "server_group_resource_id",
			Name:     "resource_name",
			ParentID: "parent_id",
		},
		Parent:   parentGroupNode,
		SlugName: "server_group_slugname",
		OutputID: "server_group_output_id",
	}

	return parser.ParsedState{
		FirewallServices: []*parser.FirewallServiceNode{
			svcNode,
		},
		FirewallZones: []*parser.FirewallZoneNode{
			zoneNode,
		},
		FirewallInterfaces: []*parser.FirewallInterfaceNode{
			ifaceNode,
		},
		FirewallPolicies: []*parser.FirewallPolicyNode{
			policyNode, policyNodeEmpty,
		},
		AlertProfiles: []*parser.AlertProfileNode{
			alertNode,
		},
		ServerGroups: []*parser.ServerGroupNode{
			parentGroupNode, groupNode,
		},
	}
}
