package output

import (
	"fmt"
	"io"
	"log"
	"text/template"

	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

const firewallZoneResource = `
resource "cphalo_firewall_zone" "{{.Zone.SlugName}}" {
  name = "{{.Zone.Resource.Name}}"
  {{- if .Zone.Resource.Description}}
  description = "{{.Zone.Resource.Description}}"
  {{- end}}
  ip_address = [
  {{- range $ip := .Zone.IPAddress}}
    "{{$ip}}",
  {{- end}}
  ]
}
`

const firewallZoneData = `
data "cphalo_firewall_zone" "{{.Zone.SlugName}}" {
  name = "{{.Zone.Resource.Name}}"
}
`

var (
	firewallZoneResourceTemplate *template.Template
	firewallZoneDataTemplate     *template.Template
)

func init() {
	var err error

	firewallZoneResourceTemplate, err = template.New("cphalo_firewall_zone_resource").Parse(firewallZoneResource)
	if err != nil {
		log.Fatalf("could not parse firewall firewallZone resource template: %v", err)
	}

	firewallZoneDataTemplate, err = template.New("cphalo_firewall_zone_data").Parse(firewallZoneData)
	if err != nil {
		log.Fatalf("could not parse firewall firewallZone data resource template: %v", err)
	}
}

func writeFirewallZones(w io.Writer, state parser.ParsedState, report *outputReport) error {
	for _, zone := range state.FirewallZones {
		if err := writeFirewallZone(w, zone, report); err != nil {
			return err
		}
	}

	return nil
}

func writeFirewallZone(w io.Writer, zone *parser.FirewallZoneNode, report *outputReport) error {
	if zone.IsOutputted {
		return nil
	}

	var tmpl *template.Template

	if zone.Resource.System {
		tmpl = firewallZoneDataTemplate
	} else {
		tmpl = firewallZoneResourceTemplate
	}

	if err := tmpl.Execute(w, struct{ Zone parser.FirewallZoneNode }{Zone: *zone}); err != nil {
		return fmt.Errorf("could not render firewall firewallZone (ID: %s) to template: %v", zone.Resource.ID, err)
	}

	if !zone.Resource.System {
		appendImportStatement(fmt.Sprintf("cphalo_firewall_zone.%s", zone.SlugName), zone.Resource.ID)
		report.firewallZone.resourceGood++
	} else {
		report.firewallZone.dataGood++
	}

	zone.IsOutputted = true

	return nil
}
