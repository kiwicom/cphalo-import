package output

import (
	"fmt"
	"io"
	"text/tabwriter"

	"github.com/olekukonko/tablewriter"

	"gitlab.com/kiwicom/cphalo-import/pkg/parser"
)

type outputReportType struct {
	dataGood int
	//dataBad      int
	resourceGood int
	resourceBad  int
}

type outputReport struct {
	firewallZone        outputReportType
	firewallPolicy      outputReportType
	firewallService     outputReportType
	firewallInterface   outputReportType
	serverGroup         outputReportType
	serverGroupFwPolicy outputReportType
	alertProfile        outputReportType

	hasSkipped bool
}

func WriteState(key, secret string, tfFile, shFile, reportBuf io.Writer, state parser.ParsedState) error {
	errorMsg := "could not write state: %v"

	clearImportStatements()

	report := new(outputReport)

	if err := writeProviderBlock(tfFile, key, secret); err != nil {
		return fmt.Errorf(errorMsg, err)
	}

	if err := writeFirewallZones(tfFile, state, report); err != nil {
		return fmt.Errorf(errorMsg, err)
	}

	if err := writeFirewallServices(tfFile, state, report); err != nil {
		return fmt.Errorf(errorMsg, err)
	}

	if err := writeFirewallInterfaces(tfFile, state, report); err != nil {
		return fmt.Errorf(errorMsg, err)
	}

	if err := writeFirewallPolicies(tfFile, state, report); err != nil {
		return fmt.Errorf(errorMsg, err)
	}

	if err := writeServerGroups(tfFile, state, report); err != nil {
		return fmt.Errorf(errorMsg, err)
	}

	if err := writeServerGroupFirewallPolicies(tfFile, state, report); err != nil {
		return fmt.Errorf(errorMsg, err)
	}

	if err := writeAlertProfiles(tfFile, state, report); err != nil {
		return fmt.Errorf(errorMsg, err)
	}

	if err := writeImportStatements(shFile); err != nil {
		return fmt.Errorf("could not write import script: %v", err)
	}

	printReport(reportBuf, report)

	return nil
}

func printReport(buffer io.Writer, report *outputReport) {
	w := new(tabwriter.Writer)

	w.Init(buffer, 16, 8, 0, '\t', tabwriter.Debug)

	defer w.Flush()

	p := func(num int) string {
		if num == 0 {
			return ""
		}
		return fmt.Sprintf("%d", num)
	}

	fmt.Fprintln(buffer, "Report:")

	table := tablewriter.NewWriter(buffer)
	table.SetColumnAlignment([]int{tablewriter.ALIGN_LEFT, tablewriter.ALIGN_CENTER, tablewriter.ALIGN_CENTER, tablewriter.ALIGN_CENTER})
	table.SetHeader([]string{"", "Resource", "Data", "Skipped"})

	data := [][]string{
		{"ServerGroup", p(report.serverGroup.resourceGood), p(report.serverGroup.dataGood), p(report.serverGroup.resourceBad)},
		{"ServerGroupFirewallPolicy", p(report.serverGroupFwPolicy.resourceGood), p(report.serverGroupFwPolicy.dataGood), p(report.serverGroupFwPolicy.resourceBad)},
		{"FirewallPolicy", p(report.firewallPolicy.resourceGood), p(report.firewallPolicy.dataGood), p(report.firewallPolicy.resourceBad)},
		{"FirewallZone", p(report.firewallZone.resourceGood), p(report.firewallZone.dataGood), p(report.firewallZone.resourceBad)},
		{"FirewallInterface", p(report.firewallInterface.resourceGood), p(report.firewallInterface.dataGood), p(report.firewallInterface.resourceBad)},
		{"FirewallService", p(report.firewallService.resourceGood), p(report.firewallService.dataGood), p(report.firewallService.resourceBad)},
		{"AlertProfile", p(report.alertProfile.resourceGood), p(report.alertProfile.dataGood), p(report.alertProfile.resourceBad)},
	}

	table.AppendBulk(data)
	table.Render()

	if report.hasSkipped {
		fmt.Fprintln(buffer, "\nSkipped resources are commented-out in your Terraform config, along with reason why they were skipped.")
	}
}
