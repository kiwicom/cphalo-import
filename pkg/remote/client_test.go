package remote_test

import (
	"net/http"
	"os"
	"testing"

	"gitlab.com/kiwicom/cphalo-import/pkg/remote"
)

type roundTripper func(req *http.Request) *http.Response

func (f roundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

func getTestClient(fn roundTripper) *http.Client {
	return &http.Client{
		Transport: fn,
	}
}

func makeHTTPResponse(t *testing.T, testFilePath string) *http.Response {
	data, err := os.Open(testFilePath)
	if err != nil {
		t.Fatalf("could not read %s test file: %v", testFilePath, err)
	}

	return &http.Response{
		StatusCode: 200,
		Body:       data,
		Header:     make(http.Header),
	}
}

func TestClient(t *testing.T) {
	client := getTestClient(func(req *http.Request) *http.Response {
		if req.URL.String() == "https://api.cloudpassage.com/oauth/access_token?grant_type=client_credentials" {
			return makeHTTPResponse(t, "./testdata/access_token.json")
		}

		if req.URL.String() == "https://api.cloudpassage.com/v1/alert_profiles" {
			return makeHTTPResponse(t, "./testdata/alert_profiles.json")
		}

		if req.URL.String() == "https://api.cloudpassage.com/v1/groups" {
			return makeHTTPResponse(t, "./testdata/server_groups.json")
		}

		if req.URL.String() == "https://api.cloudpassage.com/v1/firewall_zones" {
			return makeHTTPResponse(t, "./testdata/firewall_zones.json")
		}

		if req.URL.String() == "https://api.cloudpassage.com/v1/firewall_interfaces" {
			return makeHTTPResponse(t, "./testdata/firewall_interfaces.json")
		}

		if req.URL.String() == "https://api.cloudpassage.com/v1/firewall_services" {
			return makeHTTPResponse(t, "./testdata/firewall_services.json")
		}

		if req.URL.String() == "https://api.cloudpassage.com/v1/firewall_policies" {
			return makeHTTPResponse(t, "./testdata/firewall_policies.json")
		}

		if req.URL.String() == "https://api.cloudpassage.com/v1/firewall_policies/8cb238a0ee5511e18s9a4d1cedf20253/firewall_rules" {
			return makeHTTPResponse(t, "./testdata/firewall_rules.json")
		}

		if req.URL.String() == "https://api.cloudpassage.com/v1/firewall_policies/8cb238a0ee5511e18s9a4d1cedf20253/firewall_rules/ce3d810cee5b23ju9d0d15ea2eff2521" {
			return makeHTTPResponse(t, "./testdata/firewall_rule.json")
		}

		if req.URL.String() == "https://api.cloudpassage.com/v1/groups/9981f162c2d611e680b17f1fb185c564" {
			return makeHTTPResponse(t, "./testdata/server_groups.json")
		}

		t.Fatalf("received unexpected http call to %s", req.URL.String())

		return nil
	})

	c := remote.NewClient("", "", client)
	state := c.FetchState()

	checkAlertProfiles(t, state)
	checkServerGroups(t, state)
	checkFirewallZones(t, state)
	checkFirewallInterfaces(t, state)
	checkFirewallServices(t, state)
	checkFirewallPolicies(t, state)
}

func checkAlertProfiles(t *testing.T, state remote.State) {
	expectedAlertID := "0226a27af95c11e5a92a471a4310f7c2"
	gotAlertID := state.AlertProfiles[0].ID

	if gotAlertID != expectedAlertID {
		t.Fatalf("expected alert profile ID: %s, got: %s", expectedAlertID, gotAlertID)
	}

	if len(state.AlertProfiles) != 1 {
		t.Fatalf("expected alert profile count to be 1, got: %d", len(state.AlertProfiles))
	}
}

func checkServerGroups(t *testing.T, state remote.State) {
	expectedID := "9981f162c2d611e680b17f1fb185c564"
	gotID := state.ServerGroups[0].ID

	if gotID != expectedID {
		t.Fatalf("expected server group ID: %s, got: %s", expectedID, gotID)
	}

	if len(state.ServerGroups) != 1 {
		t.Fatalf("expected server group count to be 1, got: %d", len(state.ServerGroups))
	}
}

func checkFirewallZones(t *testing.T, state remote.State) {
	expectedID := "ea81ec609956012ee2db40989asd0980"
	gotID := state.FirewallZones[0].ID

	if gotID != expectedID {
		t.Fatalf("expected firewall zone ID: %s, got: %s", expectedID, gotID)
	}

	if len(state.FirewallZones) != 1 {
		t.Fatalf("expected firewall zone count to be 1, got: %d", len(state.FirewallZones))
	}
}

func checkFirewallInterfaces(t *testing.T, state remote.State) {
	expectedID := "eab260c09956012ee2db4087123ad87s"
	gotID := state.FirewallInterfaces[0].ID

	if gotID != expectedID {
		t.Fatalf("expected firewall interface ID: %s, got: %s", expectedID, gotID)
	}

	if len(state.FirewallInterfaces) != 1 {
		t.Fatalf("expected firewall interface count to be 1, got: %d", len(state.FirewallInterfaces))
	}
}

func checkFirewallServices(t *testing.T, state remote.State) {
	expectedID := "ea3fe1309956012ee2989ea98as989as"
	gotID := state.FirewallServices[0].ID

	if gotID != expectedID {
		t.Fatalf("expected firewall service ID: %s, got: %s", expectedID, gotID)
	}

	if len(state.FirewallServices) != 1 {
		t.Fatalf("expected firewall service count to be 1, got: %d", len(state.FirewallServices))
	}
}

func checkFirewallPolicies(t *testing.T, state remote.State) {
	expectedID := "8cb238a0ee5511e18s9a4d1cedf20253"
	gotID := state.FirewallPolicies[0].ID

	if gotID != expectedID {
		t.Fatalf("expected firewall policy ID: %s, got: %s", expectedID, gotID)
	}

	if len(state.FirewallPolicies) != 1 {
		t.Fatalf("expected firewall policy count to be 1, got: %d", len(state.FirewallPolicies))
	}

	expectedRuleID := "jtap810cee5b11e89d0d15ea2eff2521"
	gotRuleID := state.FirewallPolicies[0].FirewallRules[0].ID

	if gotRuleID != expectedRuleID {
		t.Fatalf("expected firewall policy rule ID: %s, got: %s", expectedRuleID, gotRuleID)
	}

	if len(state.FirewallPolicies[0].FirewallRules) != 1 {
		t.Fatalf("expected firewall policy rule count to be 1, got: %d", len(state.FirewallPolicies[0].FirewallRules))
	}
}
