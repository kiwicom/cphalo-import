package remote

import (
	"log"
	"net/http"

	"gitlab.com/kiwicom/cphalo-go"
)

type State struct {
	ServerGroups                []cphalo.ServerGroup
	ServerGroupFirewallPolicies []cphalo.ServerGroupFirewallPolicy
	FirewallZones               []cphalo.FirewallZone
	FirewallInterfaces          []cphalo.FirewallInterface
	FirewallServices            []cphalo.FirewallService
	FirewallPolicies            []cphalo.FirewallPolicy
	AlertProfiles               []cphalo.AlertProfile
}

type Client struct {
	api *cphalo.Client
}

func NewClient(appKey, appSecret string, client *http.Client) *Client {
	return &Client{
		api: cphalo.NewClient(appKey, appSecret, client),
	}
}

// FetchState returns the State struct populated with values from CPHalo API
func (c *Client) FetchState() State {
	groups := c.fetchServerGroups()
	return State{
		ServerGroups:                groups,
		ServerGroupFirewallPolicies: c.fetchServerGroupFirewallPolicies(groups),
		FirewallZones:               c.fetchFirewallZones(),
		FirewallInterfaces:          c.fetchFirewallInterfaces(),
		FirewallServices:            c.fetchFirewallServices(),
		FirewallPolicies:            c.fetchFirewallPolicies(),
		AlertProfiles:               c.fetchAlertProfiles(),
	}
}

func (c *Client) fetchServerGroups() []cphalo.ServerGroup {
	log.Println("Fetching server groups")

	resp, err := c.api.ListServerGroups()
	if err != nil {
		log.Fatalf("could not fetch server groups: %v", err)
	}

	return resp.Groups
}

func (c *Client) fetchServerGroupFirewallPolicies(groups []cphalo.ServerGroup) (response []cphalo.ServerGroupFirewallPolicy) {
	log.Println("Fetching server group firewall policies")

	lenGroups := len(groups)

	for idx, group := range groups {
		log.Printf("Fetching policies for server group %d/%d", idx+1, lenGroups)

		resp, err := c.api.GetServerGroupFirewallPolicy(group.ID)
		if err != nil {
			log.Fatalf("could not fetch server group firewall policies: %v", err)
		}

		response = append(response, resp.Group)
	}

	return response
}

func (c *Client) fetchFirewallZones() []cphalo.FirewallZone {
	log.Println("Fetching firewall zones")

	resp, err := c.api.ListFirewallZones()
	if err != nil {
		log.Fatalf("could not fetch firewall zones: %v", err)
	}

	return resp.Zones
}

func (c *Client) fetchFirewallInterfaces() []cphalo.FirewallInterface {
	log.Println("Fetching firewall interfaces")

	resp, err := c.api.ListFirewallInterfaces()
	if err != nil {
		log.Fatalf("could not fetch firewall interfaces: %v", err)
	}

	return resp.Interfaces
}

func (c *Client) fetchFirewallServices() []cphalo.FirewallService {
	log.Println("Fetching firewall services")

	resp, err := c.api.ListFirewallServices()
	if err != nil {
		log.Fatalf("could not fetch firewall services: %v", err)
	}

	return resp.Services
}

func (c *Client) fetchFirewallPolicies() (policies []cphalo.FirewallPolicy) {
	log.Println("Fetching firewall policies")

	resp, err := c.api.ListFirewallPolicies()
	if err != nil {
		log.Fatalf("could not fetch firewall policies: %v", err)
	}

	lenPolicies := len(resp.Policies)

	for policyIdx, policy := range resp.Policies {
		resp, err := c.api.ListFirewallRules(policy.ID)
		if err != nil {
			log.Fatalf("could not fetch firewall rules for policy %s: %v", policy.ID, err)
		}

		lenRules := len(resp.Rules)

		for ruleIdx, rule := range resp.Rules {
			log.Printf("Fetching firewall rule %d/%d for policy %d/%d", ruleIdx+1, lenRules, policyIdx+1, lenPolicies)
			resp, err := c.api.GetFirewallRule(policy.ID, rule.ID)
			if err != nil {
				log.Fatalf("could not fetch firewall rule details (policyID %s, ruleID: %s): %v", policy.ID, rule.ID, err)
			}

			policy.FirewallRules = append(policy.FirewallRules, resp.Rule)
		}

		policies = append(policies, policy)
	}

	return policies
}

func (c *Client) fetchAlertProfiles() []cphalo.AlertProfile {
	log.Println("Fetching alert profiles")

	resp, err := c.api.ListAlertProfiles()
	if err != nil {
		log.Fatalf("could not fetch alert profiles: %v", err)
	}

	return resp.AlertProfiles
}
