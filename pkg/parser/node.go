package parser

import "gitlab.com/kiwicom/cphalo-go"

type FirewallZoneNode struct {
	Resource  cphalo.FirewallZone
	OutputID  string
	Kind      string
	SlugName  string
	IPAddress []string

	IsUsed      bool
	IsOutputted bool
}

type FirewallInterfaceNode struct {
	Resource cphalo.FirewallInterface
	OutputID string
	SlugName string

	IsUsed      bool
	IsOutputted bool
}

type FirewallServiceNode struct {
	Resource cphalo.FirewallService
	OutputID string
	SlugName string

	IsUsed      bool
	IsOutputted bool
}

type FirewallRuleNode struct {
	Resource cphalo.FirewallRule
	Service  *FirewallServiceNode
	Iface    *FirewallInterfaceNode
	Source   *FirewallZoneNode
	Target   *FirewallZoneNode

	IsOutputted bool
}

type FirewallPolicyNode struct {
	Resource cphalo.FirewallPolicy
	Rules    []*FirewallRuleNode
	OutputID string
	SlugName string

	IsOutputted bool

	ErrorComment string
}

type ServerGroupNode struct {
	Resource cphalo.ServerGroup
	OutputID string
	SlugName string
	Parent   *ServerGroupNode
	Alerts   []*AlertProfileNode

	IsOutputted bool
}

type ServerGroupFirewallPolicyNode struct {
	Resource cphalo.ServerGroupFirewallPolicy
	Group    *ServerGroupNode
	Policy   *FirewallPolicyNode
	OutputID string
	SlugName string

	IsOutputted bool
}

type AlertProfileNode struct {
	Resource cphalo.AlertProfile
	OutputID string
	SlugName string

	IsOutputted bool
}
