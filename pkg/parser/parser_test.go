package parser

import (
	"testing"

	"gitlab.com/kiwicom/cphalo-go"
	"gitlab.com/kiwicom/cphalo-import/pkg/remote"
)

func TestParseState(t *testing.T) {
	remoteState := getRemoteState()
	parsedState := ParseState(remoteState)

	checkMatchingSizes(t, "firewall_zone", len(remoteState.FirewallZones), len(parsedState.FirewallZones))
	checkMatchingSizes(t, "firewall_interface", len(remoteState.FirewallInterfaces), len(parsedState.FirewallInterfaces))
	checkMatchingSizes(t, "firewall_service", len(remoteState.FirewallServices), len(parsedState.FirewallServices))
	checkMatchingSizes(t, "firewall_policy", len(remoteState.FirewallPolicies), len(parsedState.FirewallPolicies))
	checkMatchingSizes(t, "server_group", len(remoteState.ServerGroups), len(parsedState.ServerGroups))
	checkMatchingSizes(t, "alert_profile", len(remoteState.AlertProfiles), len(parsedState.AlertProfiles))

	findAlertProfileNode(
		t,
		parsedState.AlertProfiles,
		"alert_profile_data_resource",
		"${data.cphalo_alert_profile.alert_profile.id}",
		"alert profile",
		"alert_profile",
	)

	findAlertProfileNode(
		t,
		parsedState.AlertProfiles,
		"alert_profile_data_resource_2",
		"${data.cphalo_alert_profile.alert_profile_2.id}",
		"alert profile",
		"alert_profile_2",
	)

	findFirewallZoneNode(
		t,
		parsedState.FirewallZones,
		"firewall_zone_resource",
		"${cphalo_firewall_zone.fw_zone.id}",
		"fw zone",
		"fw_zone",
		"1.1.1.1,2.2.2.2,3.3.3.3",
		false,
	)

	findFirewallZoneNode(
		t,
		parsedState.FirewallZones,
		"firewall_zone_data_resource",
		"${data.cphalo_firewall_zone.fw_zone_2.id}",
		"fw zone",
		"fw_zone_2",
		"4.4.4.4",
		true,
	)

	findFirewallServiceNode(
		t,
		parsedState.FirewallServices,
		"firewall_service_resource",
		"${cphalo_firewall_service.fw_service.id}",
		"fw service",
		"fw_service",
		"ICMP",
		"",
		false,
	)

	findFirewallServiceNode(
		t,
		parsedState.FirewallServices,
		"firewall_service_resource_with_port",
		"${cphalo_firewall_service.fw_service_2.id}",
		"fw service",
		"fw_service_2",
		"UDP",
		"53",
		false,
	)

	findFirewallServiceNode(
		t,
		parsedState.FirewallServices,
		"firewall_service_data_resource",
		"${data.cphalo_firewall_service.fw_service_3.id}",
		"fw service",
		"fw_service_3",
		"",
		"",
		true,
	)

	findFirewallInterfaceNode(
		t,
		parsedState.FirewallInterfaces,
		"firewall_interface_resource",
		"${cphalo_firewall_interface.fw_interface.id}",
		"fw interface",
		"fw_interface",
		false,
	)

	findFirewallInterfaceNode(
		t,
		parsedState.FirewallInterfaces,
		"firewall_interface_data_resource",
		"${data.cphalo_firewall_interface.fw_interface_2.id}",
		"fw interface",
		"fw_interface_2",
		true,
	)

	findFirewallPolicyNode(
		t,
		parsedState.FirewallPolicies,
		"firewall_policy_resource",
		"${cphalo_firewall_policy.firewall_policy.id}",
		"firewall_policy",
	)

	findFirewallRuleNode(
		t,
		parsedState.FirewallPolicies[0].Rules,
		"firewall_rule_01",
		"INPUT",
		"ACCEPT",
		"ANY",
		1,
		"firewall_interface_data_resource",
		"firewall_service_data_resource",
		"firewall_zone_data_resource",
		"",
	)

	findFirewallRuleNode(
		t,
		parsedState.FirewallPolicies[0].Rules,
		"firewall_rule_02",
		"OUTPUT",
		"ACCEPT",
		"ANY",
		1,
		"",
		"",
		"",
		"All Server Groups",
	)

	findServerGroupNode(
		t,
		parsedState.ServerGroups,
		"server_group_parent_resource",
		"${data.cphalo_server_group.server_group_parent.id}",
		"server group parent",
		"server_group_parent",
		"",
		"",
	)

	findServerGroupNode(
		t,
		parsedState.ServerGroups,
		"server_group_resource",
		"${cphalo_server_group.server_group.id}",
		"server group",
		"server_group",
		"server_group_parent_resource",
		"alert_profile_data_resource",
	)
}

func checkMatchingSizes(t *testing.T, desc string, lenA, lenB int) {
	if lenA != lenB {
		t.Errorf("size mismatch for %s, expected len %d, got %d", desc, lenA, lenB)
	}
}

var newlineIPAddresses = `
1.1.1.1,
2.2.2.2,
3.3.3.3
`

func getRemoteState() remote.State {
	alertProfileDataResource := cphalo.AlertProfile{
		ID:   "alert_profile_data_resource",
		Name: "alert profile",
	}

	alertProfileDataResource2 := cphalo.AlertProfile{
		ID:   "alert_profile_data_resource_2",
		Name: "alert profile",
	}

	fwZoneResource := cphalo.FirewallZone{
		ID:        "firewall_zone_resource",
		Name:      "fw zone",
		System:    false,
		IPAddress: newlineIPAddresses,
	}

	fwZoneDataResource := cphalo.FirewallZone{
		ID:        "firewall_zone_data_resource",
		Name:      "fw zone",
		System:    true,
		IPAddress: "4.4.4.4",
	}

	fwServiceResource := cphalo.FirewallService{
		ID:       "firewall_service_resource",
		Name:     "fw service",
		Protocol: "ICMP",
		System:   false,
	}

	fwServiceResourceWithPort := cphalo.FirewallService{
		ID:       "firewall_service_resource_with_port",
		Name:     "fw service",
		Protocol: "UDP",
		Port:     "53",
		System:   false,
	}

	fwServiceDataResource := cphalo.FirewallService{
		ID:     "firewall_service_data_resource",
		Name:   "fw service",
		System: true,
	}

	fwInterfaceResource := cphalo.FirewallInterface{
		ID:     "firewall_interface_resource",
		Name:   "fw interface",
		System: false,
	}

	fwInterfaceDataResource := cphalo.FirewallInterface{
		ID:     "firewall_interface_data_resource",
		Name:   "fw interface",
		System: true,
	}

	fwPolicy := cphalo.FirewallPolicy{
		ID:   "firewall_policy_resource",
		Name: "firewall policy",
		FirewallRules: []cphalo.FirewallRule{
			{
				ID:                "firewall_rule_01",
				Chain:             "INPUT",
				Action:            "ACCEPT",
				Position:          1,
				ConnectionStates:  "ANY",
				FirewallInterface: &fwInterfaceDataResource,
				FirewallService:   &fwServiceDataResource,
				FirewallSource: &cphalo.FirewallRuleSourceTarget{
					ID:   fwZoneDataResource.ID,
					Kind: "FirewallZone",
				},
			},
			{
				ID:               "firewall_rule_02",
				Chain:            "OUTPUT",
				Action:           "ACCEPT",
				Position:         1,
				ConnectionStates: "ANY",
				FirewallTarget: &cphalo.FirewallRuleSourceTarget{
					ID:   "All Server Groups",
					Kind: "Group",
				},
			},
		},
	}

	serverGroupParentResource := cphalo.ServerGroup{
		ID:   "server_group_parent_resource",
		Name: "server group parent",
	}

	serverGroupResource := cphalo.ServerGroup{
		ID:       "server_group_resource",
		Name:     "server group",
		ParentID: serverGroupParentResource.ID,
		AlertProfileIDs: []string{
			alertProfileDataResource.ID,
		},
	}

	remoteState := remote.State{
		AlertProfiles: []cphalo.AlertProfile{
			alertProfileDataResource,
			alertProfileDataResource2,
		},
		FirewallZones: []cphalo.FirewallZone{
			fwZoneResource,
			fwZoneDataResource,
		},
		FirewallServices: []cphalo.FirewallService{
			fwServiceResource,
			fwServiceResourceWithPort,
			fwServiceDataResource,
		},
		FirewallInterfaces: []cphalo.FirewallInterface{
			fwInterfaceResource,
			fwInterfaceDataResource,
		},
		FirewallPolicies: []cphalo.FirewallPolicy{
			fwPolicy,
		},
		ServerGroups: []cphalo.ServerGroup{
			serverGroupParentResource,
			serverGroupResource,
		},
	}

	return remoteState
}

func findFirewallZoneNode(t *testing.T, nodes []*FirewallZoneNode, id, outputID, name, slug, ips string, system bool) {
	var node *FirewallZoneNode

	for _, n := range nodes {
		if id == n.Resource.ID {
			node = n
		}
	}

	if node == nil {
		t.Errorf("could not find fw zone with ID: %s", id)
		return
	}

	if node.Kind != "FirewallZone" {
		t.Errorf("fw zone %s: expected kind: FirewallZone, got: %s", id, node.Kind)
	}

	if node.IsOutputted {
		t.Errorf("fw zone %s: expected IsOutputted: false, got: true", id)
	}

	if id != node.Resource.ID {
		t.Errorf("fw zone %s: expected ID: %s, got: %s", id, id, node.Resource.ID)
	}

	if outputID != node.OutputID {
		t.Errorf("fw zone %s: expected OutputID: %s, got: %s", id, outputID, node.OutputID)
	}

	if name != node.Resource.Name {
		t.Errorf("fw zone %s: expected name: %s, got: %s", id, name, node.Resource.Name)
	}

	if slug != node.SlugName {
		t.Errorf("fw zone %s: expected slug: %s, got: %s", id, slug, node.SlugName)
	}

	if ips != node.Resource.IPAddress {
		t.Errorf("fw zone %s: expected ips: %s, got %s", id, ips, node.Resource.IPAddress)
	}

	if system != node.Resource.System {
		t.Errorf("fw zone %s: expected sytem bool: %t, got: %t", id, system, node.Resource.System)
	}
}

func findFirewallInterfaceNode(t *testing.T, nodes []*FirewallInterfaceNode, id, outputID, name, slug string, system bool) {
	var node *FirewallInterfaceNode

	for _, n := range nodes {
		if id == n.Resource.ID {
			node = n
		}
	}

	if node == nil {
		t.Errorf("could not find fw zone with ID: %s", id)
		return
	}

	if node.IsOutputted {
		t.Errorf("fw interface %s: expected IsOutputted: false, got: true", id)
	}

	if id != node.Resource.ID {
		t.Errorf("fw interface %s: expected ID: %s, got: %s", id, id, node.Resource.ID)
	}

	if outputID != node.OutputID {
		t.Errorf("fw interface %s: expected OutputID: %s, got: %s", id, outputID, node.OutputID)
	}

	if name != node.Resource.Name {
		t.Errorf("fw interface %s: expected name: %s, got: %s", id, name, node.Resource.Name)
	}

	if slug != node.SlugName {
		t.Errorf("fw interface %s: expected slug: %s, got: %s", id, slug, node.SlugName)
	}

	if system != node.Resource.System {
		t.Errorf("fw interface %s: expected sytem bool: %t, got: %t", id, system, node.Resource.System)
	}
}

func findFirewallServiceNode(t *testing.T, nodes []*FirewallServiceNode, id, outputID, name, slug, protocol, port string, system bool) {
	var node *FirewallServiceNode

	for _, n := range nodes {
		if id == n.Resource.ID {
			node = n
		}
	}

	if node == nil {
		t.Errorf("could not find fw service with ID: %s", id)
		return
	}

	if node.IsOutputted {
		t.Errorf("fw service %s: expected IsOutputted: false, got: true", id)
	}

	if id != node.Resource.ID {
		t.Errorf("fw service %s: expected ID: %s, got: %s", id, id, node.Resource.ID)
	}

	if outputID != node.OutputID {
		t.Errorf("fw service %s: expected OutputID: %s, got: %s", id, outputID, node.OutputID)
	}

	if name != node.Resource.Name {
		t.Errorf("fw service %s: expected name: %s, got: %s", id, name, node.Resource.Name)
	}

	if slug != node.SlugName {
		t.Errorf("fw service %s: expected slug: %s, got: %s", id, slug, node.SlugName)
	}

	if system != node.Resource.System {
		t.Errorf("fw service %s: expected sytem bool: %t, got: %t", id, system, node.Resource.System)
	}

	if protocol != node.Resource.Protocol {
		t.Errorf("fw service %s: expected protocol: %s, got: %s", id, protocol, node.Resource.Protocol)
	}

	if port != node.Resource.Port {
		t.Errorf("fw service %s: expected port: %s, got: %s", id, port, node.Resource.Port)
	}
}

func findAlertProfileNode(t *testing.T, nodes []*AlertProfileNode, id, outputID, name, slug string) {
	var node *AlertProfileNode

	for _, n := range nodes {
		if id == n.Resource.ID {
			node = n
		}
	}

	if node == nil {
		t.Errorf("could not find alert profile with ID: %s", id)
		return
	}

	if node.IsOutputted {
		t.Errorf("alert profile %s: expected IsOutputted: false, got: true", id)
	}

	if id != node.Resource.ID {
		t.Errorf("alert profile %s: expected ID: %s, got: %s", id, id, node.Resource.ID)
	}

	if outputID != node.OutputID {
		t.Errorf("alert profile %s: expected OutputID: %s, got: %s", id, outputID, node.OutputID)
	}

	if name != node.Resource.Name {
		t.Errorf("alert profile %s: expected name: %s, got: %s", id, name, node.Resource.Name)
	}

	if slug != node.SlugName {
		t.Errorf("alert profile %s: expected slug: %s, got: %s", id, slug, node.SlugName)
	}
}

func findFirewallPolicyNode(t *testing.T, nodes []*FirewallPolicyNode, id, outputID, slug string) {
	var node *FirewallPolicyNode

	for _, n := range nodes {
		if id == n.Resource.ID {
			node = n
		}
	}

	if node == nil {
		t.Errorf("could not find fw policy with ID: %s", id)
		return
	}

	if node.IsOutputted {
		t.Errorf("fw policy %s: expected IsOutputted: false, got: true", id)
	}

	if id != node.Resource.ID {
		t.Errorf("fw policy %s: expected ID: %s, got: %s", id, id, node.Resource.ID)
	}

	if outputID != node.OutputID {
		t.Errorf("fw policy %s: expected OutputID: %s, got: %s", id, outputID, node.OutputID)
	}

	if slug != node.SlugName {
		t.Errorf("fw policy %s: expected slug: %s, got: %s", id, slug, node.SlugName)
	}
}

func findFirewallRuleNode(t *testing.T, nodes []*FirewallRuleNode, id, chain, action, states string, position int, interfaceID, serviceID, sourceID, targetID string) {
	var node *FirewallRuleNode

	for _, n := range nodes {
		if id == n.Resource.ID {
			node = n
		}
	}

	if node == nil {
		t.Errorf("could not find fw rule with ID: %s", id)
		return
	}

	if node.IsOutputted {
		t.Errorf("fw rule %s: expected IsOutputted: false, got: true", id)
	}

	if id != node.Resource.ID {
		t.Errorf("fw rule %s: expected ID: %s, got: %s", id, id, node.Resource.ID)
	}

	if chain != node.Resource.Chain {
		t.Errorf("fw rule %s: expected chain: %s, got: %s", id, chain, node.Resource.Chain)
	}

	if action != node.Resource.Action {
		t.Errorf("fw rule %s: expected action: %s, got: %s", id, action, node.Resource.Action)
	}

	if states != node.Resource.ConnectionStates {
		t.Errorf("fw rule %s: expected connection_states: %s, got: %s", id, states, node.Resource.ConnectionStates)
	}

	if position != node.Resource.Position {
		t.Errorf("fw rule %s: expected position: %d, got: %d", id, position, node.Resource.Position)
	}

	if interfaceID != "" || node.Resource.FirewallInterface != nil {
		if node.Resource.FirewallInterface == nil {
			t.Errorf("fw rule %s: expected firewall interface ID: %s, got: nil", id, interfaceID)
		} else if interfaceID != node.Resource.FirewallInterface.ID {
			t.Errorf("fw rule %s: expected firewall interface ID: %s, got: %s", id, interfaceID, node.Resource.FirewallInterface.ID)
		}
	}

	if serviceID != "" || node.Resource.FirewallService != nil {
		if node.Resource.FirewallService == nil {
			t.Errorf("fw rule %s: expected firewall service ID: %s, got: nil", id, serviceID)
		} else if serviceID != node.Resource.FirewallService.ID {
			t.Errorf("fw rule %s: expected firewall service ID: %s, got: %s", id, serviceID, node.Resource.FirewallService.ID)
		}
	}

	if sourceID != "" || node.Resource.FirewallSource != nil {
		if node.Resource.FirewallSource == nil {
			t.Errorf("fw rule %s: expected firewall source ID: %s, got: nil", id, sourceID)
		} else if sourceID != node.Resource.FirewallSource.ID {
			t.Errorf("fw rule %s: expected firewall source ID: %s, got: %s", id, sourceID, node.Resource.FirewallSource.ID)
		}
	}

	if targetID != "" || node.Resource.FirewallTarget != nil {
		if node.Resource.FirewallTarget == nil {
			t.Errorf("fw rule %s: expected firewall target ID: %s, got: nil", id, targetID)
		} else if targetID != node.Resource.FirewallTarget.ID {
			t.Errorf("fw rule %s: expected firewall target ID: %s, got: %s", id, targetID, node.Resource.FirewallTarget.ID)
		}
	}

}

func findServerGroupNode(t *testing.T, nodes []*ServerGroupNode, id, outputID, name, slug, parentID, alertID string) {
	var node *ServerGroupNode

	for _, n := range nodes {
		if id == n.Resource.ID {
			node = n
		}
	}

	if node == nil {
		t.Errorf("could not find server group with ID: %s", id)
		return
	}

	if node.IsOutputted {
		t.Errorf("server group %s: expected IsOutputted: false, got: true", id)
	}

	if id != node.Resource.ID {
		t.Errorf("server group %s: expected ID: %s, got: %s", id, id, node.Resource.ID)
	}

	if outputID != node.OutputID {
		t.Errorf("server group %s: expected OutputID: %s, got: %s", id, outputID, node.OutputID)
	}

	if name != node.Resource.Name {
		t.Errorf("server group %s: expected name: %s, got: %s", id, name, node.Resource.Name)
	}

	if slug != node.SlugName {
		t.Errorf("server group %s: expected slug: %s, got: %s", id, slug, node.SlugName)
	}

	if parentID != "" || node.Parent != nil {
		if node.Parent == nil {
			t.Errorf("server group %s: expected parentID: %s, got: nil", id, parentID)
		} else if parentID != node.Parent.Resource.ID {
			t.Errorf("server group %s: expected parentID: %s, got: %s", id, parentID, node.Parent.Resource.ID)
		}
	}

	if alertID != "" || len(node.Alerts) > 0 {
		if len(node.Alerts) == 0 {
			t.Errorf("server group %s: expected alertID: %s, got: nil", id, alertID)
		} else if alertID != node.Alerts[0].Resource.ID {
			t.Errorf("server group %s: expected alertID: %s, got: %s", id, alertID, node.Alerts[0].Resource.ID)
		}
	}
}
