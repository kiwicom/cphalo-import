package parser

import (
	"fmt"
	"reflect"

	"github.com/gosimple/slug"
)

var (
	slugger = getSlugger()
)

func getSlugger() func(t interface{}, name string) string {
	var (
		slugs      = make(map[string]int)
		substitute = map[string]string{"-": "_"}
	)

	return func(t interface{}, name string) string {
		typeName := reflect.TypeOf(t).Name()
		slugName := slug.Substitute(slug.Make(name), substitute)

		combinedName := fmt.Sprintf("%s-%s", typeName, slugName)

		if _, ok := slugs[combinedName]; ok {
			slugs[combinedName]++

			return fmt.Sprintf("%s_%d", slugName, slugs[combinedName])
		}

		slugs[combinedName] = 1

		return slugName
	}
}
