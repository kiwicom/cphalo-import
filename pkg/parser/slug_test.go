package parser

import (
	"testing"
)

func TestSlugger(t *testing.T) {
	testData := []struct {
		in  string
		out string
	}{
		{
			in:  "hello world",
			out: "hello_world",
		},
		{
			in:  "hello world",
			out: "hello_world_2",
		},
		{
			in:  "please/don't_use|such(strings)",
			out: "please_dont_use_such_strings",
		},
	}

	slugger = getSlugger()

	for _, tt := range testData {
		t.Run(tt.in, func(t *testing.T) {
			got := slugger(ServerGroupNode{}, tt.in)
			if tt.out != got {
				t.Errorf("expected : %s, got: %s", tt.out, got)
			}
		})
	}
}
