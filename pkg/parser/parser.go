package parser

import (
	"fmt"
	"log"
	"regexp"
	"strings"

	"gitlab.com/kiwicom/cphalo-go"
	"gitlab.com/kiwicom/cphalo-import/pkg/remote"
)

type ParsedState struct {
	FirewallZones               []*FirewallZoneNode
	FirewallInterfaces          []*FirewallInterfaceNode
	FirewallServices            []*FirewallServiceNode
	FirewallPolicies            []*FirewallPolicyNode
	ServerGroups                []*ServerGroupNode
	ServerGroupFirewallPolicies []*ServerGroupFirewallPolicyNode
	AlertProfiles               []*AlertProfileNode
}

func ParseState(state remote.State) (parsedState ParsedState) {
	var (
		alertProfiles               = parseAlertProfiles(state)
		firewallZones               = parseFirewallZones(state)
		firewallInterfaces          = parseFirewallInterfaces(state)
		firewallServices            = parseFirewallServices(state)
		serverGroups                = parseServerGroups(state, alertProfiles)
		firewallPolicies            = parseFirewallPolicies(state, firewallInterfaces, firewallServices, serverGroups, firewallZones)
		serverGroupFirewallPolicies = parseServerGroupFirewallPolicies(state, serverGroups, firewallPolicies)
	)

	for _, zone := range firewallZones {
		if zone.IsUsed {
			parsedState.FirewallZones = append(parsedState.FirewallZones, zone)
		}
	}

	for _, iface := range firewallInterfaces {
		if iface.IsUsed {
			parsedState.FirewallInterfaces = append(parsedState.FirewallInterfaces, iface)
		}
	}

	for _, service := range firewallServices {
		if service.IsUsed {
			parsedState.FirewallServices = append(parsedState.FirewallServices, service)
		}
	}

	for _, policy := range firewallPolicies {
		parsedState.FirewallPolicies = append(parsedState.FirewallPolicies, policy)
	}

	for _, group := range serverGroups {
		parsedState.ServerGroups = append(parsedState.ServerGroups, group)
	}

	for _, groupPolicy := range serverGroupFirewallPolicies {
		parsedState.ServerGroupFirewallPolicies = append(parsedState.ServerGroupFirewallPolicies, groupPolicy)
	}

	for _, alert := range alertProfiles {
		parsedState.AlertProfiles = append(parsedState.AlertProfiles, alert)
	}

	return parsedState
}

func parseServerGroups(state remote.State, alertProfiles map[string]*AlertProfileNode) map[string]*ServerGroupNode {
	nodes := make(map[string]*ServerGroupNode)

	for _, r := range state.ServerGroups {
		slugName := slugger(r, r.Name)

		outputID := fmt.Sprintf("${cphalo_server_group.%s.id}", slugName)
		if r.ParentID == "" {
			outputID = fmt.Sprintf("${data.cphalo_server_group.%s.id}", slugName)
		}

		groupNode := &ServerGroupNode{
			Resource: r,
			OutputID: outputID,
			SlugName: slugName,
		}

		for _, alertID := range r.AlertProfileIDs {
			groupNode.Alerts = append(groupNode.Alerts, alertProfiles[alertID])
		}

		nodes[r.ID] = groupNode
	}

	// create links between server groups
	for _, group := range nodes {
		if group.Resource.ParentID != "" {
			group.Parent = nodes[group.Resource.ParentID]
		}
	}

	return nodes
}

func parseServerGroupFirewallPolicies(state remote.State, groups map[string]*ServerGroupNode, policies map[string]*FirewallPolicyNode) map[string]*ServerGroupFirewallPolicyNode {
	var nodes = make(map[string]*ServerGroupFirewallPolicyNode)

	for _, groupPolicy := range state.ServerGroupFirewallPolicies {
		if groupPolicy.LinuxFirewallPolicyID == "" {
			continue
		}

		group, ok := groups[groupPolicy.GroupID]
		if !ok {
			log.Fatalf("could not find server group with ID %s", groupPolicy.GroupID)
		}

		policy, ok := policies[string(groupPolicy.LinuxFirewallPolicyID)]
		if !ok {
			log.Fatalf("could not find firewall policy with ID %s", groupPolicy.LinuxFirewallPolicyID)
		}

		slugName := slugger(groupPolicy, group.Resource.Name)

		nodes[groupPolicy.GroupID] = &ServerGroupFirewallPolicyNode{
			Resource: groupPolicy,
			Policy:   policy,
			Group:    group,
			OutputID: fmt.Sprintf("${cphalo_server_group_firewall_policy.%s.id}", slugName),
			SlugName: slugName,
		}
	}

	return nodes
}

func parseFirewallPolicies(
	state remote.State,
	ifaces map[string]*FirewallInterfaceNode,
	svcs map[string]*FirewallServiceNode,
	groups map[string]*ServerGroupNode,
	zones map[string]*FirewallZoneNode) map[string]*FirewallPolicyNode {
	var (
		allActiveServers   = "All Active Servers"
		allGhostPortsUsers = "All GhostPorts users"
		nodes              = make(map[string]*FirewallPolicyNode)
	)

	for _, policy := range state.FirewallPolicies {
		slugName := slugger(policy, policy.Name)

		policyNode := &FirewallPolicyNode{
			Resource: policy,
			OutputID: fmt.Sprintf("${cphalo_firewall_policy.%s.id}", slugName),
			SlugName: slugName,
		}

		if len(policy.FirewallRules) == 0 {
			policyNode.ErrorComment = "Error: contains no firewall rules. Import skipped."
		}

		for _, rule := range policy.FirewallRules {
			ruleNode := &FirewallRuleNode{
				Resource: rule,
			}

			if rule.FirewallInterface != nil {
				ifaceNode := ifaces[rule.FirewallInterface.ID]
				ifaceNode.IsUsed = true
				ruleNode.Iface = ifaceNode
			}

			if rule.FirewallService != nil {
				svcNode := svcs[rule.FirewallService.ID]
				svcNode.IsUsed = true
				ruleNode.Service = svcNode
			}

			// CPHalo allows special zones which have no ID:
			// Name: "All Active Servers" Kind: "Group"
			// Name: "All GhostPorts users" Kind: "UserGroup"
			getZoneNode := func(id, name, kind string) *FirewallZoneNode {
				zoneNode, ok := zones[id]
				if !ok {
					groupNode, ok := groups[id]
					if ok {
						zoneNode = &FirewallZoneNode{
							Resource: cphalo.FirewallZone{
								ID: name,
							},
							Kind:        kind,
							OutputID:    groupNode.OutputID,
							IsOutputted: true, // skip writing resource block
						}

						zoneNode.IsUsed = true

						return zoneNode
					}

					if strings.EqualFold(name, allActiveServers) {
						name = allActiveServers
					}
					if strings.EqualFold(name, allGhostPortsUsers) {
						name = allGhostPortsUsers
					}

					zoneNode = &FirewallZoneNode{
						Resource: cphalo.FirewallZone{
							ID: name,
						},
						Kind:        kind,
						OutputID:    name,
						IsOutputted: true, // skip writing resource block
					}
				}

				zoneNode.IsUsed = true

				return zoneNode
			}

			if rule.FirewallSource != nil {
				ruleNode.Source = getZoneNode(rule.FirewallSource.ID, rule.FirewallSource.Name, rule.FirewallSource.Kind)
			}

			if rule.FirewallTarget != nil {
				ruleNode.Target = getZoneNode(rule.FirewallTarget.ID, rule.FirewallTarget.Name, rule.FirewallTarget.Kind)
			}

			policyNode.Rules = append(policyNode.Rules, ruleNode)
		}

		nodes[policy.ID] = policyNode
	}

	return nodes
}

func parseAlertProfiles(state remote.State) map[string]*AlertProfileNode {
	nodes := make(map[string]*AlertProfileNode)

	for _, r := range state.AlertProfiles {
		slugName := slugger(r, r.Name)

		nodes[r.ID] = &AlertProfileNode{
			Resource: r,
			OutputID: fmt.Sprintf("${data.cphalo_alert_profile.%s.id}", slugName),
			SlugName: slugName,
		}
	}

	return nodes
}

func parseFirewallServices(state remote.State) map[string]*FirewallServiceNode {
	nodes := make(map[string]*FirewallServiceNode)

	for _, r := range state.FirewallServices {
		slugName := slugger(r, r.Name)

		outputID := fmt.Sprintf("${cphalo_firewall_service.%s.id}", slugName)
		if r.System {
			outputID = fmt.Sprintf("${data.cphalo_firewall_service.%s.id}", slugName)
		}

		nodes[r.ID] = &FirewallServiceNode{
			Resource: r,
			IsUsed:   !r.System,
			OutputID: outputID,
			SlugName: slugName,
		}
	}

	return nodes
}

func parseFirewallInterfaces(state remote.State) map[string]*FirewallInterfaceNode {
	nodes := make(map[string]*FirewallInterfaceNode)

	for _, r := range state.FirewallInterfaces {
		slugName := slugger(r, r.Name)

		outputID := fmt.Sprintf("${cphalo_firewall_interface.%s.id}", slugName)
		if r.System {
			outputID = fmt.Sprintf("${data.cphalo_firewall_interface.%s.id}", slugName)
		}

		nodes[r.ID] = &FirewallInterfaceNode{
			Resource: r,
			IsUsed:   !r.System,
			OutputID: outputID,
			SlugName: slugName,
		}
	}

	return nodes
}

func parseFirewallZones(state remote.State) map[string]*FirewallZoneNode {
	newlineRegex, err := regexp.Compile(`\s+`)
	if err != nil {
		log.Fatalf("could not compile newline regex: %v", err)
	}

	nodes := make(map[string]*FirewallZoneNode)

	for _, r := range state.FirewallZones {
		slugName := slugger(r, r.Name)

		outputID := fmt.Sprintf("${cphalo_firewall_zone.%s.id}", slugName)
		if r.System {
			outputID = fmt.Sprintf("${data.cphalo_firewall_zone.%s.id}", slugName)
		}

		r.IPAddress = newlineRegex.ReplaceAllString(r.IPAddress, "")

		nodes[r.ID] = &FirewallZoneNode{
			Resource:  r,
			IsUsed:    !r.System,
			Kind:      "FirewallZone",
			OutputID:  outputID,
			SlugName:  slugName,
			IPAddress: strings.Split(r.IPAddress, ","),
		}
	}

	return nodes
}
