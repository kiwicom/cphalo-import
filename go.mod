module gitlab.com/kiwicom/cphalo-import

require (
	github.com/gosimple/slug v1.4.2
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/olekukonko/tablewriter v0.0.1
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	gitlab.com/kiwicom/cphalo-go v0.1.2-0.20190307200223-ee1ce7d2b50f
)
