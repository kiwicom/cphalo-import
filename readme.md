# CPHalo Import tool

A command line tool for creating Terraform configuration files from existing CPHalo infrastructure.

[![pipeline status](https://gitlab.com/kiwicom/cphalo-import/badges/master/pipeline.svg)](https://gitlab.com/kiwicom/cphalo-import/pipelines)
[![coverage report](https://gitlab.com/kiwicom/cphalo-import/badges/master/coverage.svg)](https://gitlab.com/kiwicom/cphalo-import/commits/master)

## Usage

### Install

```bash
go get gitlab.com/kiwicom/cphalo-import/cmd/cphalo-import
```

### Generate Terraform config files.

```bash
cphalo-import run \
    --key APP_KEY \
    --secret APP_SECRET \
    --script-file import.sh \
    --terraform-file main.tf
```

This command will iterate over existing CPHalo resources, writing required Terraform resource statements into `main.tf`, as well as writing import statements into `import.sh`.

After command successfully runs, you can execute `./import.sh` to import remote state into local Terraform state.

> **Warning!**  
>
> `terraform-provider-cphalo` does not allow firewall policies without firewall rules. If such a resource exists in your existing CPHalo configuration, please either fix this issue by removing the firewall policy or assign a benign firewall rule to the policy.  
> If you choose to keep the empty firewall policy, your generated Terraform configuration file will have it commented-out.

## Supported command

List supported commands with `help`:

```bash
cphalo-import -h
``` 

## Docker usage

```bash
docker run --rm -it  \
    -v `pwd`/output:/output \
    --workdir /output \
    registry.gitlab.com/kiwicom/cphalo-import run \
    --key APP_KEY \
    --secret APP_SECRET \
    --script-file import.sh \
    --terraform-file main.tf
```
